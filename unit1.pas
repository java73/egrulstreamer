unit Unit1;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Buttons, egrul_streamer, egrul_interface, LCLIntf, Grids;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    RegionFilter: TCheckBox;
    RegionSelector: TComboBox;
    Label3: TLabel;
    NameEdit: TEdit;
    IDEdit: TEdit;
    CaptchaImg: TImage;
    Label1: TLabel;
    Label2: TLabel;
    SearchType: TRadioGroup;
    Grid: TStringGrid;
    procedure BitBtn1Click(Sender: TObject);
  private
    EGRULStream: TEGRULStreamer;
    procedure RefreshCaptcha;
    function RecognizeFunc(captcha: TStream): string;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  RefreshCaptcha;
end;

procedure TForm1.RefreshCaptcha;
var
  PdfStream: TMemoryStream;
  LegalsList: TCollection;
  i: integer;
  RegF: string;
begin
  EGRULStream := TEGRULStreamer.Create;
  try
    try
      if SearchType.ItemIndex=0 then begin
          EGRULStream.GetExtractByOGRN(IDEdit.Text, @RecognizeFunc, True, TStream(PdfStream));
          PdfStream.SaveToFile('extract.pdf');
          OpenDocument('extract.pdf');
      end
      else
      begin
         regF := '';
         if RegionFilter.Checked then regF := LeftStr(RegionSelector.Text,2);
         EGRULStream.GetLegalsListByName(NameEdit.Text,regF,@RecognizeFunc,LegalsList);
         Grid.RowCount:=LegalsList.Count;
         for i:=0 to LegalsList.Count-1 do
           begin
              Grid.Cells[0,i]:=InttoStr(i);
              Grid.Cells[1,i]:=TEGRULItem(LegalsList.Items[i]).NAME;
              Grid.Cells[2,i]:=TEGRULItem(LegalsList.Items[i]).ADRESTEXT;
           end;
         LegalsList.Free;
      end;
    except
      on E: Exception do ShowMessage(E.Message);
    end;
  finally
    EGRULStream.Free;
  end;
end;

function TForm1.RecognizeFunc(captcha: TStream): string;
begin
  CaptchaImg.Picture.LoadFromStream(captcha);
  Result := InputBox('Капча','Введите текст капчи с картинки', '');
end;

end.

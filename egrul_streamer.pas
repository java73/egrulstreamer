unit egrul_streamer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, egrul_interface,
  httpsend, ssl_openssl, synautil, SAX_HTML, DOM_HTML, DOM,
  fpjson, fpjsonrtti;

type

  { TEGRULStreamer }

  TEGRULStreamer = class(TInterfacedObject, IEGRULStreamer)
  private
    HTTPSender: THTTPSend;
    Doc: THTMLDocument;
    Inputs: TDOMNodeList;
    captchaURL, captchaToken, captcha, Params: string;
    function GetCaptchaToken: string;
    procedure PrepareHeaders;
    function GetLegalsList: TCollection;
    procedure ProcessCaptcha(CaptchaFunc: TCapthcaRecognizeFunc);
  public
    procedure GetExtractByOGRN(OGRN: string; CaptchaFunc: TCapthcaRecognizeFunc;
      isLegal: boolean; var Extract: TStream);
    procedure GetLegalsListByName(Name, Region: string; CaptchaFunc: TCapthcaRecognizeFunc;
      var LegalsList: TCollection);
    destructor Destroy; override;
  end;

implementation

{ TEGRULStreamer }

function TEGRULStreamer.GetCaptchaToken: string;
var
  i: integer;
begin
  Inputs := Doc.GetElementsByTagName('input');
  i := 0;
  if Inputs.Count > 0 then
    while (i < Inputs.Count) do
    begin
      if (TDOMElement(Inputs[i]).GetAttribute('name') = 'captchaToken') then
      begin
        Result := TDOMElement(Inputs[i]).GetAttribute('value');
        Inputs.Free;
        Doc.Free;
        exit;
      end;
      Inc(i);
    end;
  raise Exception.Create('Отсутствует токен капчи на странице ИФНС');
end;

procedure TEGRULStreamer.PrepareHeaders;
begin
  HTTPSender.Headers.Clear;
  HTTPSender.Document.Clear;
  HTTPSender.Protocol := '1.1';
  HTTPSender.UserAgent := 'Mozilla/5.0 (Windows NT 6.1) Chrome/67.0.3396.99';
  HTTPSender.Headers.Text := HEADERS_NALOG;
  HTTPSender.MimeType := 'application/x-www-form-urlencoded';
end;

function TEGRULStreamer.GetLegalsList: TCollection;
var
  JStream: TJSONDeStreamer;
  jData: TJSONData;
begin
  JStream := TJSONDeStreamer.Create(nil);
  Result := TCollection.Create(TEGRULItem);
  jData := GetJSON(HTTPSender.Document);
  try
    JStream.JSONToCollection(jData.GetPath('rows'), Result);
  finally
    jData.Free;
    jStream.Free;
  end;
end;

procedure TEGRULStreamer.ProcessCaptcha(CaptchaFunc: TCapthcaRecognizeFunc);
begin
  if HTTPSender = nil then
    HTTPSender := THTTPSend.Create;
  if not HTTPSender.HTTPMethod('GET', EGRUL_URL) then
    raise Exception.Create('Сайт ИФНС не открывается');
  ReadHTMLFile(Doc, HTTPSender.Document);
  captchaToken := GetCaptchaToken;
  captchaURL := EGRUL_URL + 'static/captcha.html?a=' + captchaToken + '&version=2';
  if not HTTPSender.HTTPMethod('GET', captchaURL) then
    raise Exception.Create('Невозможно скачать изображение капчи');
  captcha := CaptchaFunc(HTTPSender.Document);
  PrepareHeaders;
  if captcha = '%%bad%%' then
    raise Exception.Create('Капча не распознана');
end;

procedure TEGRULStreamer.GetExtractByOGRN(OGRN: string;
  CaptchaFunc: TCapthcaRecognizeFunc; isLegal: boolean; var Extract: TStream);
begin
  ProcessCaptcha(CaptchaFunc);
  if isLegal then Params := 'kind=ul' else Params := 'kind=fl';
  Params += '&srchUl=ogrn&srchFl=ogrn&ogrninnul=';
  if isLegal then Params += OGRN;
  Params += '&namul=&regionul=&ogrninnfl=';
  if not isLegal then Params += OGRN;
  Params += '&fam=&nam=&otch=&region&captcha=' + captcha + '&captchaToken=' + captchaToken;
  WriteStrToStream(HTTPSender.Document, Params);
  if not HTTPSender.HTTPMethod('POST', EGRUL_URL) then
    raise Exception.Create('Сайт ИФНС не открывается');
  HTTPSender.Headers.Clear;
  if HTTPSender.HTTPMethod('GET', TEGRULItem(GetLegalsList.Items[0]).GetPdfLink) then
    Extract := HTTPSender.Document
  else
    Extract := nil;
end;

procedure TEGRULStreamer.GetLegalsListByName(Name, Region: string;
  CaptchaFunc: TCapthcaRecognizeFunc; var LegalsList: TCollection);
begin
  ProcessCaptcha(CaptchaFunc);
  Params := 'kind=ul&srchUl=name&srchFl=ogrn&ogrninnul=&namul=';
  Params += Name + '&regionul=' + Region + '&ogrninnfl=&fam=&nam=&otch=&region';
  Params += '&captcha=' + captcha + '&captchaToken=' + captchaToken;
  WriteStrToStream(HTTPSender.Document, Params);
  if not HTTPSender.HTTPMethod('POST', EGRUL_URL) then
    raise Exception.Create('Сайт ИФНС не открывается');
  LegalsList := GetLegalsList;
end;

destructor TEGRULStreamer.Destroy;
begin
  HTTPSender.Free;
end;

end.

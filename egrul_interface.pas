unit egrul_interface;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  EGRUL_URL = 'https://egrul.nalog.ru/';
  HEADERS_NALOG =
    'Accept: application/json, text/javascript, */*; q=0.01' + #13#10 +
    'Accept-Encoding: gzip, deflate, br' + #13#10 +
    'Accept-Language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7' + #13#10 +
    'Connection: keep-alive' + #13#10 +
    'Host: egrul.nalog.ru' + #13#10 +
    'Origin: https://egrul.nalog.ru' + #13#10 +
    'Referer: https://egrul.nalog.ru/' + #13#10 +
    'X-Requested-With: XMLHttpRequest';

type

  // callback для функции распознавания капчи, на входе - поток с изображением,
  // на выходе - строка с результатом распознавания; в случае ошибки функция
  // должна вернуть в строке '%%bad%%'
  TCapthcaRecognizeFunc = function(Captha: TStream): string of object;

  { TEGRULItem }

  // класс, описывающий структуру из JSON результата, который выдает сайт ИФНС
  TEGRULItem = class(TCollectionItem)
  private
    fT, fINN, fNAME, fOGRN, fADRESTEXT, fCNT, fDTREG, fDTEND, fKPP: string;
  public
    function GetPdfLink: string;
  published
    property T: string read fT write fT;
    property INN: string read fINN write fINN;
    property NAME: string read fNAME write fNAME;
    property OGRN: string read fOGRN write fOGRN;
    property ADRESTEXT: string read fADRESTEXT write fADRESTEXT;
    property CNT: string read fCNT write fCNT;
    property DTREG: string read fDTREG write fDTREG;
    property DTEND: string read fDTEND write fDTEND;
    property KPP: string read fKPP write fKPP;
  end;

  IEGRULstreamer = interface
    ['{16AE2647-FFD8-4631-B808-F1BED1D236DC}']
    procedure GetExtractByOGRN(OGRN: string; CaptchaFunc: TCapthcaRecognizeFunc;
      isLegal: boolean; var Extract: TStream);
    procedure GetLegalsListByName(Name, Region: string; CaptchaFunc: TCapthcaRecognizeFunc;
      var LegalsList: TCollection);
  end;

implementation

{ TEGRULItem }

function TEGRULItem.GetPdfLink: string;
begin
  Result := EGRUL_URL + 'download/' + T;
end;

end.
